package main;

import controller.Implementations.LoginControllerImpl;
import model.Implementations.CellBuilder;
import model.Implementations.GuardBuilder;
import model.Interfaces.Cell;
import model.Interfaces.Guard;
import utils.ResourcesFactory;
import view.Interfaces.LoginView;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

/**
 * The main of the application.
 */
public final class Main {

	/**
     * Program main, this is the "root" of the application.
     * @param args
     * unused,ignore
     */
	public static void main(final String... args) {
	    initResources();
	    new LoginControllerImpl(new LoginView());
	}

    private static void initResources(){
        new File(ResourcesFactory.RESOURCES_DIRECTORY).mkdir();
        if(new File(ResourcesFactory.GUARDS_FILE).length() == 0){
            try {
                initializeGuards();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(new File(ResourcesFactory.CELLS_FILE).length() == 0){
            try {
                initializeCells();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static void initializeCells() throws IOException {
        List<Cell> cellsList = new ArrayList<>();
        IntStream.range(0, 20).forEach(i -> cellsList.add(new CellBuilder().setId(i)
                                                                           .setPosition("Primo piano")
                                                                           .setCapacity(4)
                                                                           .createCell()));
        IntStream.range(20, 40).forEach(i -> cellsList.add(new CellBuilder().setId(i)
                                                                            .setPosition("Secondo piano")
                                                                            .setCapacity(3)
                                                                            .createCell()));
        IntStream.range(40, 45).forEach(i -> cellsList.add(new CellBuilder().setId(i)
                                                                            .setPosition("Terzo piano")
                                                                            .setCapacity(4)
                                                                            .createCell()));
        IntStream.range(45, 50).forEach(i -> cellsList.add(new CellBuilder().setId(i)
                                                                            .setPosition("Piano sotterraneo, celle di isolamento")
                                                                            .setCapacity(1)
                                                                            .createCell()));

        ResourcesFactory.setCells(cellsList);
    }

    private static void initializeGuards() throws IOException {
        String pattern = "MM/dd/yyyy";
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = format.parse("01/01/1980");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        List<Guard> guardsList = new ArrayList<>();
        guardsList.add(new GuardBuilder().setName("Oronzo")
                                         .setSurname("Cantani")
                                         .setBirthDate(date)
                                         .setRank(1)
                                         .setTelephoneNumber("0764568")
                                         .setGuardId(1)
                                         .setPassword("ciao01")
                                         .createGuard());
        guardsList.add(new GuardBuilder().setName("Emile")
                                         .setSurname("Heskey")
                                         .setBirthDate(date)
                                         .setRank(2)
                                         .setTelephoneNumber("456789")
                                         .setGuardId(2)
                                         .setPassword("asdasd")
                                         .createGuard());
        guardsList.add(new GuardBuilder().setName("Gennaro")
                                         .setSurname("Alfieri")
                                         .setBirthDate(date)
                                         .setRank(3)
                                         .setTelephoneNumber("0764568")
                                         .setGuardId(3)
                                         .setPassword("qwerty")
                                         .createGuard());

        ResourcesFactory.setGuards(guardsList);
    }
}
