package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;
import model.Interfaces.Prisoner;
import model.Interfaces.Visitor;
import utils.ResourcesFactory;
import view.Interfaces.AddVisitorsView;
import view.Interfaces.MoreFunctionsView;

/**
 * controller della addVisitorsView
 */
public class AddVisitorsControllerImpl {
	
	static AddVisitorsView visitorsView;
	
	/**
	 * costruttore
	 * @param view la view
	 */
	public AddVisitorsControllerImpl(AddVisitorsView view)
	{
		AddVisitorsControllerImpl.visitorsView=view;
		visitorsView.addBackListener(new BackListener());
		visitorsView.addInsertVisitorListener(new InsertListener());
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			visitorsView.dispose();
			new MoreFunctionsControllerImpl(new MoreFunctionsView(visitorsView.getRank()));
		}
		
	}
	
	/**
	 * listener che gestisce l'inserimento di visitatori
	 */
	public static class InsertListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {

			//recupero la lista dei visitatori e la salvo in una lista
			List<Visitor> visitors = null;
			try {
				visitors = ResourcesFactory.getVisitors();
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			//salvo il visitatore inserito nella view
			Visitor vi = visitorsView.getVisitor();
			//controllo che non ci siano errori
			try {
				if(vi.getName().length()<2||vi.getSurname().length()<2 || !checkPrisonerID(vi))
					visitorsView.displayErrorMessage("Devi inserire un nome, un cognome e un prigioniero esistente");
				else{
					//inserisco il visitatore nella lista
					visitors.add(vi);
					visitorsView.displayErrorMessage("Visitatore inserito");
				}
				} catch (ClassNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
				//salvo la lista aggiornata
            try {
                ResourcesFactory.setVisitors(visitors);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }
		}
	
	/**
	 * controlla se l'id del prigioniero inserita è corretta
	 * @param v visitatore
	 * @return true se l'id è corretto
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static boolean checkPrisonerID(Visitor v) throws ClassNotFoundException, IOException{
		List<Prisoner> lista = ResourcesFactory.getCurrentPrisoners();
		boolean found = false;
		//ciclo tutti i prigionieri
		for(Prisoner p : lista)
		{
			//se gli id conicidono restituisco true
			if(p.getIdPrigioniero() == v.getPrisonerID())
				{
					found=true;
					return found;
				}
			else
				continue;
		}
		return false;
	}
}



