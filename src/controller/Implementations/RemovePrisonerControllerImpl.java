package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import controller.Interfaces.RemovePrisonerController;
import model.Interfaces.Cell;
import model.Interfaces.Prisoner;
import utils.ResourcesFactory;
import view.Interfaces.MainView;
import view.Interfaces.RemovePrisonerView;

/**
 * controller della remove prisoner view
 */
public class RemovePrisonerControllerImpl implements RemovePrisonerController{

	static RemovePrisonerView removePrisonerView;
	
	/**
	 * costruttore
	 * @param removePrisonerView la view
	 */
	public RemovePrisonerControllerImpl(RemovePrisonerView removePrisonerView){
		RemovePrisonerControllerImpl.removePrisonerView=removePrisonerView;
		removePrisonerView.addRemoveListener(new RemoveListener());
		removePrisonerView.addBackListener(new BackListener());
	}
	
	/**
	 * listener che si occupa di rimuovare il prigioniero
	 */
	public class RemoveListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			removePrisoner();
		}
		
	
	}
	
	public void removePrisoner(){

		boolean found=false;
		List<Prisoner> currentPrisoners= new ArrayList<>();
		try {
			//salvo la lista dei prigionieri correnti
			currentPrisoners= ResourcesFactory.getCurrentPrisoners();
		} catch (ClassNotFoundException | IOException e1) {
			e1.printStackTrace();
		}
		//ciclo tutti i prigionieri
		for(Prisoner p : currentPrisoners){
			//se l'id corrisponde elimino il prigioniero
			if(p.getIdPrigioniero()==removePrisonerView.getID()){
				currentPrisoners.remove(p);
				removePrisonerView.displayErrorMessage("Prigioniero rimosso");
				//recupero la lista di celle
                List<Cell>list= null;
                try {
                    list = ResourcesFactory.getCells();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                for(Cell c : list){
					if(p.getCellID()==c.getId()){
						//diminuisco il numero di prigionieri correnti nella cella del prigioniero rimosso
						c.setCurrentPrisoners(c.getCurrentPrisoners()-1);
					}
				}
				try {
					//salvo la lista aggiornata di celle
					ResourcesFactory.setCells(list);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				found=true;
				break;
			}
		}

        try {
            ResourcesFactory.setCurrentPrisoners(currentPrisoners);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if(!found){
			removePrisonerView.displayErrorMessage("Prigioniero non trovato");
		}
	}
	
	/**
	 * listener che apre la view precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			removePrisonerView.dispose();
			new MainControllerImpl(new MainView(removePrisonerView.getRank()));
		}
		
	}
	
	
}
