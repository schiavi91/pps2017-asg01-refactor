package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import view.Interfaces.InsertPrisonerView;
import view.Interfaces.LoginView;
import view.Interfaces.MainView;
import view.Interfaces.MoreFunctionsView;
import view.Interfaces.RemovePrisonerView;
import view.Interfaces.SupervisorFunctionsView;
import view.Interfaces.ViewPrisonerView;

/**
 * controller dela main view
 */
public class MainControllerImpl {

	MainView mainView;
	
	/**
	 * costruttore
	 * @param mainView la view
	 */
	public MainControllerImpl(MainView mainView){
		this.mainView=mainView;
		mainView.addLogoutListener(new LogoutListener());
		mainView.addInsertPrisonerListener(new InsertPrisonerListener());
		mainView.addRemovePrisonerListener(new RemovePrisonerListener());
		mainView.addViewPrisonerListener(new ViewPrisonerListener());
		mainView.addMoreFunctionsListener(new MoreFunctionsListener());
		mainView.addSupervisorListener(new SupervisorListener());
	}
		
	/**
	 * listener che fa tornare alla login view
	 */
	public class LogoutListener implements ActionListener{
			
		@Override
		public void actionPerformed(ActionEvent arg0) {
			mainView.dispose();
			new LoginControllerImpl(new LoginView());
			 
		}
	}
	
	/**
	 * listener che fa andare alla insert prisoner view
	 */
	public class InsertPrisonerListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			mainView.dispose();
			new InsertPrisonerControllerImpl(new InsertPrisonerView(mainView.getRank()));
		}
	}
	
	/**
	 * listener che fa andare alla remove prisoner view
	 */
	public class RemovePrisonerListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 mainView.dispose();
			 new RemovePrisonerControllerImpl(new RemovePrisonerView(mainView.getRank()));
		}
	}
	
	/**
	 * listener che fa andare alla view prisoner view
	 */
	public class ViewPrisonerListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 mainView.dispose();
			 new ViewPrisonerControllerImpl(new ViewPrisonerView(mainView.getRank()));
		}
	}
	
	/**
	 * listener che fa andare alla more functions view
	 */
	public class MoreFunctionsListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 mainView.dispose();
			 new MoreFunctionsControllerImpl(new MoreFunctionsView(mainView.getRank()));
		}
	}
		
	/**
	 * listener che fa andare alla supervisoner functions view
	 */
	public class SupervisorListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			 mainView.dispose();
			 new SupervisorControllerImpl(new SupervisorFunctionsView(mainView.getRank()));
		}
	}

}
