package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.List;

import model.Interfaces.Guard;
import utils.ResourcesFactory;
import view.Interfaces.LoginView;
import view.Interfaces.MainView;

/**
 * controller della login view
 */
public class LoginControllerImpl {
	
	LoginView loginView;

	/**
	 * costruttore
	 * @param loginView la view
	 */
	public LoginControllerImpl(LoginView loginView){
		this.loginView=loginView;
		loginView.addLoginListener(new LoginListener());
		loginView.displayErrorMessage("accedere con i profili: \n id:3 , password:qwerty \n id:2 , password:asdasd ");
	}
	
	/**
	 * listener che si occupa di effettuare il login
	 */
	public class LoginListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
						
			List<Guard> guards = null;
			try {
				//salvo la lista delle guardie nel file
				guards = ResourcesFactory.getGuards();
			} catch (ClassNotFoundException | IOException e) {
				e.printStackTrace();
			}
			boolean isInside=false;

			//controllo che non ci siano errori
			if(loginView.getUsername().isEmpty() || loginView.getPassword().isEmpty()){
				loginView.displayErrorMessage("Devi inserire username e password");
			}
			else{
				for (Guard g : guards){
					if(loginView.getUsername().equals(String.valueOf(g.getUsername())) && loginView.getPassword().equals(g.getPassword())){
						//effettuo il login
						isInside=true;
						loginView.displayErrorMessage("Benvenuto Utente "+ loginView.getUsername());	
						loginView.dispose();
						new MainControllerImpl(new MainView(g.getRank()));
					}
				}
				if(isInside==false){
					loginView.displayErrorMessage("Combinazione username/password non corretta");
				}
				isInside = false;
			}
		}
		
	}
}
