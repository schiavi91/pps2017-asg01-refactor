package controller.Implementations;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import controller.Interfaces.InsertPrisonerController;
import model.Implementations.PrisonerBuilder;
import model.Interfaces.Cell;
import model.Interfaces.Prisoner;
import utils.ResourcesFactory;
import view.Interfaces.InsertPrisonerView;
import view.Interfaces.MainView;

/**
 * controller della view insertprisoner
 */
public class InsertPrisonerControllerImpl implements InsertPrisonerController{

	static InsertPrisonerView insertPrisonerView;
	
	/**
	 * costruttore
	 * @param insertPrisonerView la view
	 */
	public InsertPrisonerControllerImpl(InsertPrisonerView insertPrisonerView) {
		InsertPrisonerControllerImpl.insertPrisonerView=insertPrisonerView;
		insertPrisonerView.addInsertPrisonerListener(new InsertPrisonerListener());
		insertPrisonerView.addBackListener(new BackListener());
		insertPrisonerView.addAddCrimeListener(new AddCrimeListener());
		
	}
	
	public void insertPrisoner(){

		boolean correct=true;
		 List<Prisoner>prisoners=new ArrayList<>();
		 List<Prisoner>currentPrisoners=new ArrayList<>();
		 //recupero le liste di prigionieri correnti e storici
		 try {
			prisoners= ResourcesFactory.getPrisoners();
			currentPrisoners=ResourcesFactory.getCurrentPrisoners();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		 	//recupero il prigioniero inserito nella view
			Prisoner p = new PrisonerBuilder().setName(insertPrisonerView.getName1())
                                              .setSurname(insertPrisonerView.getSurname1())
                                              .setBirthDate(insertPrisonerView.getBirth1())
                                              .setPrisonerId(insertPrisonerView.getPrisonerID1())
                                              .setStart(insertPrisonerView.getStart1())
                                              .setEnd(insertPrisonerView.getEnd1())
                                              .setCrimes(insertPrisonerView.getList())
                                              .setCellId(insertPrisonerView.getCellID())
                                              .createPrisoner();
			
			//controllo che non ci siano errori
			if(isSomethingEmpty(p)){
				insertPrisonerView.displayErrorMessage("Completa tutti i campi");
			}
			else{
				for(Prisoner p1 : prisoners){
					if(p1.getIdPrigioniero()==p.getIdPrigioniero()){
						insertPrisonerView.displayErrorMessage("ID già usato");
						correct=false;
					}
				}
				Calendar today = Calendar.getInstance();
				today.set(Calendar.HOUR_OF_DAY, 0); //
				if(correct==true){
				if(p.getInizio().after(p.getFine())||p.getInizio().before(today.getTime())||p.getBirthDate().after(today.getTime())){
					insertPrisonerView.displayErrorMessage("Correggi le date");
				}
				else{
					//aggiungo nuovo prigioniero in entrambe le liste
					prisoners.add(p);
					currentPrisoners.add(p);
					//recupero le celle salvate
                    List<Cell>list= null;
                    try {
                        list = ResourcesFactory.getCells();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (ClassNotFoundException e) {
                        e.printStackTrace();
                    }
                    //controllo che la cella inserita non sia piena
					for(Cell c : list){
						if(p.getCellID()==c.getId()||p.getCellID()<0||p.getCellID()>49){
							if(c.getCapacity()==c.getCurrentPrisoners()){
								insertPrisonerView.displayErrorMessage("Prova con un'altra cella");
								return;
								}
						//aggiungo un membro alla cella
						c.setCurrentPrisoners(c.getCurrentPrisoners()+1);

						}
					}
					try {
						//salvo la lista di celle aggiornata
						ResourcesFactory.setCells(list);
					} catch (IOException e1) {
						e1.printStackTrace();
					}
					//salvo le liste di prigionieri aggiornate
                    try {
                        ResourcesFactory.setPrisoners(prisoners);
                        ResourcesFactory.setCurrentPrisoners(currentPrisoners);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
					insertPrisonerView.displayErrorMessage("Prigioniero inserito");
					insertPrisonerView.setList(new ArrayList<String>());
				}
				}
			}
	}
	
	/**
	 * listener che si occupa di inserire prigionieri
	 */
	public class InsertPrisonerListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertPrisoner();
		}
		
	}
	
	/**
	 * listener che fa tornare alla pagina precedente
	 */
	public class BackListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			insertPrisonerView.dispose();
			new MainControllerImpl(new MainView(insertPrisonerView.getRank()));
		}
		
	}
	
	/**
	 * listener che aggiunge un crimine alla lista dei crimini del prigioniero
	 */
	public class AddCrimeListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			//recupero i crimini inseriti nella view
			List<String>list=insertPrisonerView.getList();
			//controllo che il crimine inserito non fosse gia presente
			if(list.contains(insertPrisonerView.getCombo())){
				insertPrisonerView.displayErrorMessage("Crimine già inserito");
			}
			else{
				//inserisco il crimine
				list.add(insertPrisonerView.getCombo());
				insertPrisonerView.setList(list);
			}
		}
		
	}
	
	public boolean isSomethingEmpty(Prisoner p){
		if(p.getName().isEmpty()||p.getSurname().isEmpty()||p.getCrimini().size()==1){
			return true;
		}
		return false;
	}
}
