package model.Implementations;

import java.util.Date;

public class GuardBuilder {
    private String name;
    private String surname;
    private Date birthDate;
    private int rank;
    private String telephoneNumber;
    private int guardId;
    private String password;

    public GuardBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public GuardBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public GuardBuilder setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public GuardBuilder setRank(int rank) {
        this.rank = rank;
        return this;
    }

    public GuardBuilder setTelephoneNumber(String telephoneNumber) {
        this.telephoneNumber = telephoneNumber;
        return this;
    }

    public GuardBuilder setGuardId(int guardId) {
        this.guardId = guardId;
        return this;
    }

    public GuardBuilder setPassword(String password) {
        this.password = password;
        return this;
    }

    public GuardImpl createGuard() {
        return new GuardImpl(name, surname, birthDate, rank, telephoneNumber, guardId, password);
    }
}
