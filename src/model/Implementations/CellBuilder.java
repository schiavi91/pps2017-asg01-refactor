package model.Implementations;

public class CellBuilder {
    private int id;
    private String position;
    private int capacity;

    public CellBuilder setId(int id) {
        this.id = id;
        return this;
    }
    public CellBuilder setPosition(String position) {
        this.position = position;
        return this;
    }
    public CellBuilder setCapacity(int capacity) {
        this.capacity = capacity;
        return this;
    }

    public CellImpl createCell() {
        return new CellImpl(id, position, capacity);
    }
}
