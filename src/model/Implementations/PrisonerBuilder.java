package model.Implementations;

import java.util.Date;
import java.util.List;

public class PrisonerBuilder {
    private String name;
    private String surname;
    private Date birthDate;
    private int prisonerId;
    private Date start;
    private Date end;
    private List<String> crimes;
    private int cellId;

    public PrisonerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public PrisonerBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public PrisonerBuilder setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public PrisonerBuilder setPrisonerId(int prisonerId) {
        this.prisonerId = prisonerId;
        return this;
    }

    public PrisonerBuilder setStart(java.util.Date start) {
        this.start = start;
        return this;
    }

    public PrisonerBuilder setEnd(java.util.Date end) {
        this.end = end;
        return this;
    }

    public PrisonerBuilder setCrimes(java.util.List<String> crimes) {
        this.crimes = crimes;
        return this;
    }

    public PrisonerBuilder setCellId(int cellId) {
        this.cellId = cellId;
        return this;
    }

    public PrisonerImpl createPrisoner() {
        return new PrisonerImpl(name, surname, birthDate, prisonerId, start, end, crimes, cellId);
    }
}
