package model.Implementations;

import java.util.Date;

public class VisitorBuilder {
    private String name;
    private String surname;
    private Date birthDate;
    private int prisonerId;

    public VisitorBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public VisitorBuilder setSurname(String surname) {
        this.surname = surname;
        return this;
    }

    public VisitorBuilder setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
        return this;
    }

    public VisitorBuilder setPrisonerID(int prisonerId) {
        this.prisonerId = prisonerId;
        return this;
    }

    public VisitorImpl createVisitor() {
        return new VisitorImpl(name, surname, birthDate, prisonerId);
    }
}
