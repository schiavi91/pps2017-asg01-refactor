package utils;

import model.Implementations.CellBuilder;
import model.Implementations.GuardBuilder;
import model.Interfaces.*;

import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

public class ResourcesFactory {
    public static final String RESOURCES_DIRECTORY = "res";
    public static final String GUARDS_FILE = RESOURCES_DIRECTORY+"/GuardieUserPass.txt";
    public static final String CELLS_FILE = RESOURCES_DIRECTORY+"/Celle.txt";
    private static final String VISITORS_FILE = RESOURCES_DIRECTORY+"/Visitors.txt";
    private static final String PRISONERS_FILE = RESOURCES_DIRECTORY+"/Prisoners.txt";
    private static final String CURRENT_PRISONERS_FILE = RESOURCES_DIRECTORY+"/CurrentPrisoners.txt";
    private static final String MOVEMENTS_FILE = RESOURCES_DIRECTORY+"/AllMovements.txt";

    public static void setCells(List<Cell> list) throws IOException {
        File file = new File(CELLS_FILE);
        new ResourcesManager<Cell>().writeResource(file, list);
    }

    public static void setGuards(List<Guard> list) throws IOException {
        File file = new File(GUARDS_FILE);
        new ResourcesManager<Guard>().writeResource(file, list);
    }

    public static void setVisitors(List<Visitor> list) throws IOException {
        File file = new File(VISITORS_FILE);
        new ResourcesManager<Visitor>().writeResource(file, list);
    }

    public static void setPrisoners(List<Prisoner> list) throws IOException {
        File file = new File(PRISONERS_FILE);
        new ResourcesManager<Prisoner>().writeResource(file, list);
    }

    public static void setCurrentPrisoners(List<Prisoner> list) throws IOException {
        File file = new File(CURRENT_PRISONERS_FILE);
        new ResourcesManager<Prisoner>().writeResource(file, list);
    }

    public static void setMovements(List<Movement> list) throws IOException {
        File file = new File(MOVEMENTS_FILE);
        new ResourcesManager<Movement>().writeResource(file, list);
    }

    public static List<Cell> getCells() throws IOException, ClassNotFoundException {
        File file = new File(CELLS_FILE);
        return new ResourcesManager<Cell>().readResource(file);
    }

    public static List<Guard> getGuards() throws IOException, ClassNotFoundException {
        File file = new File(GUARDS_FILE);
        return new ResourcesManager<Guard>().readResource(file);
    }

    public static List<Visitor> getVisitors() throws IOException, ClassNotFoundException {
        File file = new File(VISITORS_FILE);
        return new ResourcesManager<Visitor>().readResource(file);
    }

    public static List<Prisoner> getPrisoners() throws IOException, ClassNotFoundException {
        File file = new File(PRISONERS_FILE);
        return new ResourcesManager<Prisoner>().readResource(file);
    }

    public static List<Prisoner> getCurrentPrisoners() throws IOException, ClassNotFoundException{
        File file = new File(CURRENT_PRISONERS_FILE);
        return new ResourcesManager<Prisoner>().readResource(file);
    }

    public static List<Movement> getMovements() throws IOException, ClassNotFoundException{
        File file = new File(MOVEMENTS_FILE);
        return new ResourcesManager<Movement>().readResource(file);
    }
}
