package utils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

class ResourcesManager<T> {

    List<T> readResource(File file) throws IOException, ClassNotFoundException {
        List<T> list = new ArrayList<>();
        if(file.length()!= 0) {
            FileInputStream fileInputStream = new FileInputStream(file);
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            while(fileInputStream.available() > 0) {
                list.add((T) objectInputStream.readObject());
            }
            objectInputStream.close();
            fileInputStream.close();
        }
        return list;
    }

    void writeResource(File file, List<T> list) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.flush();
        fileOutputStream.flush();
        for(T object : list) {
            objectOutputStream.writeObject(object);
        }
        objectOutputStream.close();
        fileOutputStream.close();
    }
}
